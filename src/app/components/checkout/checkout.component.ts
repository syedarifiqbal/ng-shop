import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../../services/order.service';
import { CartService } from '../../services/cart.service';
import { Router } from '@angular/router';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  form;
  proccesing = false;

  constructor(
    public authservice: AuthService, 
    private orderService: OrderService,
    private cartService: CartService,
    private router: Router,
    private messageService: MessageService) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(this.authservice.currentUser.email + 'gmail.com', [Validators.required, Validators.email]),
      name: new FormControl(this.authservice.currentUser.name, Validators.required),
      address: new FormControl(null, Validators.required),
      state: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      zipcode: new FormControl(null, Validators.required),
      country: new FormControl(null, Validators.required),
    });
  }

  submitOrder(){
    
    this.proccesing = true;

    let data = this.form.value;
    data.products = this.cartService.products;
    data.amount = this.cartService.total();

    this.orderService.addOrder(this.form.value).subscribe(order => {
      this.proccesing = false;
      this.messageService.add("Thank you for your order.").show();
      this.cartService.distroy();
      this.router.navigate(['/home']);
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

}
