import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/interfaces/product';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  products: Product[];

  constructor(private routeSnapShoot: ActivatedRoute,
    private productService: ProductService) { }

  ngOnInit() {
    this.routeSnapShoot.params.subscribe(query => {
      const q = query.query;
      this.productService.searchProducts(q).subscribe(data => {
        this.products = data.data;
      });
    });
  }

}
