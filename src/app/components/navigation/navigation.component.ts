import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { AuthService } from '../../services/auth.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  cartCounter = 0;
  profileDropdown = false;
  query = '';

  constructor(
    public cartService: CartService,
    public authService: AuthService,
    public message: MessageService,
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private storeService: StoreService) {  }

  ngOnInit() {

    this.storeService.getCountries().subscribe(countries => {
      console.log(countries);
    });

    this.router.events
      .subscribe( event => {
        if (event instanceof NavigationEnd) {
          const paths = event.url.split('/');
          if (paths[1] === 'search') {
              this.query = paths[2];
          } else {
            this.query = '';
          }
        }
      });
    this.activatedRouter.params.subscribe(q => console.log(q));
  }

  search(value) {
    this.router.navigate(['/search', value]);
  }

}
