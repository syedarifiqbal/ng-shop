import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import {  } from '@types/googlemaps';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  map;
  lat;
  lng;
  mapboxgl = mapboxgl;
  countries;
  stores;
  constructor(private storeService: StoreService) { }

  ngOnInit() {
    this.initializeMap();
    this.storeService.get().subscribe(data => {
      this.stores = data.data;
      this.setMarkersOnMap();
    });
    this.storeService.getCountries().subscribe(data => {
      this.countries = data;
    });
  }

  setMarkersOnMap(): any {
    const bounds = new google.maps.LatLngBounds();
      this.stores.forEach(store => {
        const marker = new google.maps.Marker({
          position: {lat: store.lat, lng: store.lng},
          map: this.map,
          title: store.name
        });

        bounds.extend(marker.getPosition());

        marker.addListener('click', () => {
          if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
          } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        });
      });
      // end foreach

      if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
        const extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
        const extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
        bounds.extend(extendPoint1);
        bounds.extend(extendPoint2);
      }
      this.map.fitBounds(bounds);
  }

  initializeMap(): any {
    const mapProp = {
      center: new google.maps.LatLng(18.5793, 73.8143),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    // this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

    navigator.geolocation.getCurrentPosition((location) => {
      const latitude = location.coords.latitude;
      const longitude = location.coords.longitude;

      const current = { lat: latitude, lng: longitude };
      const coords = [ { lat: 24.847490, lng: 67.123558 }, { lat: 24.818165, lng: 67.112265 }, { lat: 24.820188, lng: 67.144881 }];
      const mapprop = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.gmapElement.nativeElement, mapprop);

    });
  }

  setCurrentLocation() {

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
       this.lat = position.coords.latitude;
       this.lng = position.coords.longitude;
       console.log(position.coords, this.lat, this.lng);
       this.map.flyTo({
         center: [this.lat, this.lng]
       });
     });
   }
  }

  onCountryClick(country) {
    this.storeService.findByCountryName(country).subscribe(stores => {
      this.stores = stores;
      this.setMarkersOnMap();
    });
  }

  distance(lat1, lon1, lat2, lon2, unit) {
    const radlat1 = Math.PI * lat1 / 180;
    const radlat2 = Math.PI * lat2 / 180;
    const radlon1 = Math.PI * lon1 / 180;
    const radlon2 = Math.PI * lon2 / 180;
    const theta = lon1 - lon2;
    const radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit === 'K') { dist = dist * 1.609344; }
    if (unit === 'N') { dist = dist * 0.8684; }
    return dist;
  }

}
