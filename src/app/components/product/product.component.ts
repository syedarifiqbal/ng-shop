import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product;
  @Output() cartAdded = new EventEmitter();

  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit() {
  }

  addToCard() {
    this.cartAdded.emit(this.product._id);
    this.cartService.addProduct(this.product);
    // console.log(`${this.product._id} is added to your card`);
  }
  show(id) {
    this.router.navigate(['/products', id]);
  }

}
