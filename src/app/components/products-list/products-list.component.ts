import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/interfaces/product';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  products: Product[];

  constructor(private productSerivce: ProductService) { }

  ngOnInit() {
    this.productSerivce.get().subscribe( data => {
      this.products = <Product[]> data.data;
    });
    // this.products = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
  }

}
