import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-product-single',
  templateUrl: './product-single.component.html',
  styleUrls: ['./product-single.component.css']
})
export class ProductSingleComponent implements OnInit {

  product;

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService, private cartService: CartService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      const _id = params.productId
      this.productService.findById(_id).subscribe(product => this.product = product);
    });
  }

  addToCart() {
    this.cartService.addProduct(this.product);
  }

}
