import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  proccesing = false;

  constructor(private authService: AuthService, private route: Router) { }

  ngOnInit() {
  }

  login(values) {
    this.proccesing = true;

    this.signin(values);
  }

  signup(values) {
    this.proccesing = true;
    this.authService.signup(values).subscribe(data => {

      if (Object.keys(data).length === 0) {
        return;
      }

      const creadentials = {
        email: values.email,
        password: values.password
      };

      this.signin(creadentials);

    }, error => { console.log(error, 'thanks  '); });
    this.proccesing = false;
  }

  signin(createntials) {
    this.authService.login(createntials)
    .subscribe(data => {
      this.proccesing = false;
      window.localStorage.setItem('token', data.token);
      this.authService.currentUser = data.user;
      this.route.navigate([ this.authService.redirectUrl || '/home' ]);
    }, error => {
      this.proccesing = false;
      console.log(error);
    });
  }

}
