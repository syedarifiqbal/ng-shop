import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  messages: string[] = [];
  level: string;

  add(message: string) {
    this.messages.push(message);
    return this;
  }

  clear() {
    this.messages = [];
  }

  show(type = 'success') {
    this.level = type;
    setTimeout(() => {
      this.clear();
    }, 5000);
  }
}
