import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, RequestOptionsArgs, Response, Request, Headers, XHRBackend } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user';
import { map, catchError } from 'rxjs/operators';
import { HandleError, HttpErrorHandler } from './http.error.handler.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser: User;
  isLoggedIn = new BehaviorSubject<boolean>(false);
  redirectUrl: string;
  private handleError: HandleError;

  constructor( private http: HttpClient, private router: Router, private httpErrorHandler: HttpErrorHandler) {

    this.handleError = httpErrorHandler.createHandleError('ProductsService');

    this.http.get(`${environment.baseUrl}/user/profile`, { headers: environment.defaultHeader })
      .subscribe(user => {
        if (!user) {
          this.isLoggedIn.next(false);
          return;
        }

        this.currentUser = <User>user;
        this.isLoggedIn.next(true);

        if (this.redirectUrl) {
          this.router.navigate([this.redirectUrl]);
        }

    },
    error => {
      if (error.status) {
        // this.router.navigate(['/login']);
      }
    });
  }

  login($credentials): Observable<any> {
    return this.http.post(`${environment.baseUrl}/user/signin`, $credentials)
    .pipe(
        map(token => <Object>token)
    );
  }

  signup(user: User): Observable<any> {
    return this.http.post(`${environment.baseUrl}/user/signup`, user)
    .pipe(
      catchError(this.handleError('signup'))
    );
  }

  checkAuthentication() {
    if (this.isAuthenticated()) {
      return;
    }
    this.http.get(`${environment.baseUrl}/user/profile`, {headers: environment.defaultHeader})
    .subscribe(user => this.currentUser = <User>user);
  }

  logout() {
    this.currentUser = null;
    window.localStorage.removeItem('token');
    this.router.navigate(['/home']);
  }

  isAuthenticated() {
    return !!this.currentUser;
  }
}
