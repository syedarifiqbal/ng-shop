import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Product } from '../interfaces/product';
import { HttpErrorHandler, HandleError } from './http.error.handler.service';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
    providedIn: 'root'
})
export class StoreService {
  storeUrl = `${environment.baseUrl}/stores`;  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('StoreService');
  }

  /** GET Store from the server */
  get (): Observable<any> {
    return this.http.get<any>(this.storeUrl)
      .pipe(
        catchError(this.handleError('getStore', {}))
      );
  }

  /** GET Store from the server */
  findByCountryName(countryName: String): Observable<any> {
    return this.http.get<any>(`${this.storeUrl}//country/${countryName}`)
      .pipe(
        catchError(this.handleError('findByCountryName', {}))
      );
  }

  /** GET Store Countries from the server */
  getCountries(): Observable<any> {
    return this.http.get<any>(`${this.storeUrl}/countries`)
      .pipe(
        catchError(this.handleError('getCountries', {}))
      );
  }

  /* GET Store whose name contains search term */
  searchStore(term: string): Observable<any> {
    term = term.trim();

    // Add safe, URL encoded search parameter if there is a search term
    const options = term ?
     { params: new HttpParams().set('q', term) } : {};

    return this.http.get<Product[]>(`${this.storeUrl}/q`, options)
      .pipe(
        catchError(this.handleError<any>('searchStore', []))
      );
  }

  //////// Save methods //////////

  /** POST: add a new hero to the database */
  addHero (hero: Product): Observable<Product> {
    return this.http.post<Product>(this.storeUrl, hero, httpOptions)
      .pipe(
        catchError(this.handleError('addHero', hero))
      );
  }

  /** DELETE: delete the hero from the server */
  deleteHero (id: number): Observable<{}> {
    const url = `${this.storeUrl}/${id}`; // DELETE api/Store/42
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError('deleteHero'))
      );
  }

  /** PUT: update the hero on the server. Returns the updated hero upon success. */
  updateHero (hero: Product): Observable<Product> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'my-new-auth-token');

    return this.http.put<Product>(this.storeUrl, hero, httpOptions)
      .pipe(
        catchError(this.handleError('updateHero', hero))
      );
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
