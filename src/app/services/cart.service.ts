import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public products = [];

  constructor(private messageService: MessageService) {
    const products = JSON.parse(window.localStorage.getItem('products'));
    if (products) {
      this.products = products;
    }
  }

  count() {
    return this.products ? this.products.length : 0;
  }

  get(): any {
    return this.products;
  }

  distroy(): any {
    this.products = null;
    window.localStorage.removeItem('products');
  }

  total(): Number {
    let count = 0;
    this.products.forEach(product => {
      count += product.qty * product.price;
    });
    return count;
  }

  addProduct(product) {
    const existingProductsInCart = this.products.filter(p => p._id === product._id);
    if ( !! existingProductsInCart.length ) {
      this.products.forEach(p => {
        if ( p._id === product._id ) {
          p.qty += 1;
        }
      });
    } else {
      product.qty = 1;
      this.products.push(product);
    }
    window.localStorage.setItem('products', JSON.stringify(this.products));
    this.messageService.add('Product Added to Cart').show();
  }

}
