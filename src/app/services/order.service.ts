import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Product } from '../interfaces/product';
import { HttpErrorHandler, HandleError } from './http.error.handler.service';
import { environment } from 'src/environments/environment';
import { Order } from '../interfaces/order';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': `Bearer ${window.localStorage.getItem('token')}`
  })
};

@Injectable({
    providedIn: 'root'
})
export class OrderService {
  productsUrl = `${environment.baseUrl}/orders`;  // URL to web api
  private handleError: HandleError;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('OrderService');
  }

  /** GET Products from the server */
  get (): Observable<any> {
    return this.http.get<any>(this.productsUrl)
      .pipe(
        catchError(this.handleError('getProducts', {}))
      );
  }

  //////// Save methods //////////

  /** POST: add a new hero to the database */
    addOrder(order: Order): Observable<Order> {
        return this.http.post<Order>(this.productsUrl, order, httpOptions)
      .pipe(
          catchError(this.handleError('addOrder', order))
      );
  }

  /** DELETE: delete the hero from the server */
  deleteHero (id: number): Observable<{}> {
    const url = `${this.productsUrl}/${id}`; // DELETE api/Products/42
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(this.handleError('deleteHero'))
      );
  }

  /** PUT: update the hero on the server. Returns the updated hero upon success. */
  updateHero (hero: Product): Observable<Product> {
    httpOptions.headers =
      httpOptions.headers.set('Authorization', 'my-new-auth-token');

    return this.http.put<Product>(this.productsUrl, hero, httpOptions)
      .pipe(
        catchError(this.handleError('updateHero', hero))
      );
  }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
