export interface Product {
    _id: String;
    name: String;
    description: String;
    amount: Number;
    meta: {};
}
