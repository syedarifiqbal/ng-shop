export interface User {
    name: String;
    email: String;
    active: Boolean;
    password: String;
}
