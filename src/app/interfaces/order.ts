export interface Order {
    _id: String;
    date: Date;
    description: String;
    amount: Number;
    city: String;
    state: String;
    zipcode: String;
    country: String;
    email: String;
    name: String;
    address: String;
}
