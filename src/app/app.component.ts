import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { BreadCrumb } from './interfaces/bread-crumb';
import { filter, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ng-shop';
  preloader = false;

  breadcrumbs$;

  constructor(private route: Router, private activatedRoute: ActivatedRoute) {
    this.route.events.subscribe(event => {
      if ( event instanceof NavigationStart ) {
        this.preloader = true;
      }
      if ( event instanceof NavigationEnd ) {
        this.preloader = false;
      }
    });
  }

  ngOnInit() {
    this.route.events.subscribe(event => {
      if ( event instanceof NavigationStart ) {
        // this.preloader = true;
        // console.log(this.activatedRoute.snapshot.params);
        // this.activatedRoute.data.subscribe(x => console.log(x));
      }
      if ( event instanceof NavigationEnd ) {
        // this.preloader = false;
        // console.log(this.activatedRoute.snapshot.params);
        // this.activatedRoute.data.subscribe(x => console.log(x));
      }
    });
    // this.breadcrumbs$ = this.route.events.pipe(filter(event => event instanceof NavigationEnd).distinctUntilChanged())
    // .map(event =>  this.buildBreadCrumb(this.activatedRoute.root));
    // Build your breadcrumb starting with the root route of your current activated route
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '',
                breadcrumbs: Array<BreadCrumb> = []): Array<BreadCrumb> {
    // If no routeConfig is avalailable we are on the root path
    const label = route.routeConfig ? route.routeConfig.data[ 'breadcrumb' ] : 'Home';
    const path = route.routeConfig ? route.routeConfig.path : '';
    // In the routeConfig the complete path is not available,
    // so we rebuild it each time
    const nextUrl = `${url}${path}/`;
    const breadcrumb = {
        label: label,
        url: nextUrl
    };
    const newBreadcrumbs = [ ...breadcrumbs, breadcrumb ];
    // if (route.firstChild) {
    //     //If we are not on our current path yet,
    //     //there will be more children to look after, to build our breadcumb
    //     return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    // }
    return newBreadcrumbs;
  }
}
