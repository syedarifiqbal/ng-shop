import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { ProductService } from './services/product.service';
import { HttpErrorHandler } from './services/http.error.handler.service';
import { MessageService } from './services/message.service';
import { CartService } from './services/cart.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StoreComponent } from './components/about/store.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LoginComponent } from './components/login/login.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductComponent } from './components/product/product.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ProductSingleComponent } from './components/product-single/product-single.component';
import { SearchComponent } from './components/search/search.component';
import { PreloaderComponent } from './components/preloader/preloader.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StoreComponent,
    NavigationComponent,
    LoginComponent,
    ProductsListComponent,
    ProductComponent,
    CartComponent,
    CheckoutComponent,
    ProductSingleComponent,
    SearchComponent,
    PreloaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ ProductService, HttpErrorHandler, MessageService, CartService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
