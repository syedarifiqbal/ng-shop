export const environment = {
  production: true,
  baseUrl: 'http://localhost:3000/api',
  defaultHeader: {
    'Content-Type':  'application/json',
    'Authorization': `Bearer ${window.localStorage.getItem('token')}`
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYXJpZmlxYmFsIiwiYSI6ImNqbzh4MGN3ZzBkdXQza3AweDRjODFyMnYifQ.MSKUjUIsWkV-W5bbapQP0g'
  }
};
